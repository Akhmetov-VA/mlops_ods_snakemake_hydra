rule all:
    input:
        expand("models/model_{preproc}_{model}.pkl", preproc=["basic", "advanced"], model=["logreg", "rf"])

rule download_heart_dataset:
    output:
        "data/raw_data.csv"
    shell:
        """
        kaggle datasets download -d fedesoriano/heart-failure-prediction -p data
        unzip -o data/heart-failure-prediction.zip -d data
        mv data/heart.csv {output}
        rm data/heart-failure-prediction.zip
        """

rule preprocess_basic:
    input:
        "data/raw_data.csv"
    output:
        "data/preprocessed_basic.csv"
    shell:
        "python src/preproc/preprocess_basic.py {input} {output}"

rule preprocess_advanced:
    input:
        "data/raw_data.csv"
    output:
        "data/preprocessed_advanced.csv"
    shell:
        "python src/preproc/preprocess_advanced.py {input} {output}"

rule train_model:
    input:
        preprocess="data/preprocessed_{preproc}.csv"
    output:
        "models/model_{preproc}_{model}.pkl"
    params:
        model_script=lambda wildcards: f"src/train/train_{wildcards.model}.py"
    threads: 4  # Использовать до 4 потоков, настройте в соответствии с вашими ресурсами
    shell:
        "python {params.model_script} {input.preprocess} {output}"
