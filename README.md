# Проект Анализа Сердечной Недостаточности

## Описание проекта

Данный проект предназначен для анализа данных о сердечной недостаточности с использованием машинного обучения. В проекте используется набор данных от Kaggle, который содержит информацию о пациентах, страдающих сердечной недостаточностью. Основная цель проекта — построить модели предсказания сердечной недостаточности на основе различных клинических показателей.

## Данные

Данные включают в себя следующие показатели:
- Возраст
- Пол
- Тип боли в груди
- Артериальное давление в покое
- Уровень холестерина
- Максимальная частота сердцебиения
- Результаты электрокардиографии в покое
- и другие

## Установка и настройка проекта

Для работы с проектом вам понадобится установить зависимости и настроить окружение. Все зависимости и необходимые библиотеки описаны в файле `pyproject.toml`.

### Шаги для настройки проекта:

1. Установите `poetry`, систему управления зависимостями и пакетами в Python. Установка `poetry`:
    ```bash
    pip install poetry
    ```

2. В корне проекта выполните команду для установки всех зависимостей:
    ```bash
    poetry install
    ```

3. Установите `pre-commit` для автоматического выполнения линтеров и форматтеров перед коммитом:
    ```bash
    pre-commit install
    ```

## Использование Snakemake

Snakemake — это инструмент для автоматизации рабочих процессов, который позволяет воспроизводить анализ данных и моделирование с максимальной точностью и минимальными усилиями.

### Запуск пайплайна

Для запуска полного пайплайна обработки данных и обучения моделей используйте команду:
```bash
snakemake --cores all
```
Эта команда запустит все правила, описанные в Snakefile, обработает данные, обучит модели и сохранит результаты.

### Визуализация DAG (Directed Acyclic Graph)
Для того чтобы визуализировать граф зависимостей вашего пайплайна, выполните:
```bash
snakemake --dag | dot -Tpng > reports/dag.png
```
Это создаст изображение dag.png, которое покажет визуально все шаги пайплайна и их зависимости.

DAG по текущему проекту выглядит так:
![plot](./reports/dag.png)

## Описание шагов

### Правило `all`

Это правило указывает конечные цели пайплайна, которые Snakemake должен достичь. В данном случае это модели, обученные на двух разных предобработках данных (`basic` и `advanced`) для двух разных моделей машинного обучения (`logreg` и `rf`).

```yaml
rule all:
    input:
        expand("models/model_{preproc}_{model}.pkl", preproc=["basic", "advanced"], model=["logreg", "rf"])
```

### Правило download_heart_dataset
Это правило отвечает за скачивание датасета из интернета, распаковку и подготовку данных для дальнейшей обработки.

```yaml
rule download_heart_dataset:
    output:
        "data/raw_data.csv"
    shell:
        """
        kaggle datasets download -d fedesoriano/heart-failure-prediction -p data
        unzip -o data/heart-failure-prediction.zip -d data
        mv data/heart.csv {output}
        rm data/heart-failure-prediction.zip
        """
```

### Правило preprocess_basic
Это правило обрабатывает данные базовым методом предобработки. Результаты сохраняются в файл data/preprocessed_basic.csv

```yaml
rule preprocess_basic:
    input:
        "data/raw_data.csv"
    output:
        "data/preprocessed_basic.csv"
    shell:
        "python src/preproc/preprocess_basic.py {input} {output}"
```
### Правило preprocess_advanced
Правило выполняет более сложную предобработку данных. Обработанные данные сохраняются в файл data/preprocessed_advanced.csv

```yaml
rule preprocess_advanced:
    input:
        "data/raw_data.csv"
    output:
        "data/preprocessed_advanced.csv"
    shell:
        "python src/preproc/preprocess_advanced.py {input} {output}"
```

### Правило train_model
Обучает модели машинного обучения в зависимости от типа предобработки и выбранной модели. Использует до 4 потоков для ускорения процесса обучения и ограничение по кол-ву потребляемых ресурсов.

```yaml
rule train_model:
    input:
        preprocess="data/preprocessed_{preproc}.csv"
    output:
        "models/model_{preproc}_{model}.pkl"
    params:
        model_script=lambda wildcards: f"src/train/train_{wildcards.model}.py"
    threads: 4  # Использовать до 4 потоков, настройте в соответствии с вашими ресурсами
    shell:
        "python {params.model_script} {input.preprocess} {output}"
```
