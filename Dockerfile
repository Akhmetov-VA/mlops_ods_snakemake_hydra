# Use an official Python 3.11 runtime as a parent image
FROM python:3.11-slim as base

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /usr/src/app
COPY . /app

# Install Poetry
# Prevent Poetry from creating a virtual environment inside the Docker container
RUN apt-get update && apt-get install -y git && pip install "poetry==1.8.2" && poetry config virtualenvs.create false

# Production image
FROM base as production
RUN poetry install --no-dev

# CMD ["python", "test.py"]


# Development image
FROM base as development
RUN poetry install
# EXPOSE 8888

# Add a volume to store Jupyter notebooks
# VOLUME /app/notebooks

# ENTRYPOINT ["jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--no-browser", "--allow-root", "--NotebookApp.notebook_dir=/app/notebooks"]
