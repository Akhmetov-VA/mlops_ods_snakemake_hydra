import argparse

import pandas as pd
from category_encoders import WOEEncoder
from feature_engine.outliers import Winsorizer
from sklearn.compose import ColumnTransformer
from sklearn.feature_selection import SelectFromModel
from sklearn.impute import KNNImputer, SimpleImputer
from sklearn.linear_model import LassoCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler, PowerTransformer


def main(input_filepath, output_filepath):
    data = pd.read_csv(input_filepath)
    features = data.drop(columns=["HeartDisease"])

    numeric_features = features.select_dtypes(include=["int64", "float64"]).columns
    categorical_features = features.select_dtypes(include=["object"]).columns

    numeric_pipeline = Pipeline(
        steps=[
            ("imputer", KNNImputer(n_neighbors=5)),
        ]
    )

    categorical_pipeline = Pipeline(
        steps=[
            ("imputer", SimpleImputer(strategy="constant", fill_value="missing")),
            ("woe_encoder", WOEEncoder(randomized=True)),
        ]
    )

    preprocessor = ColumnTransformer(
        transformers=[
            ("num", numeric_pipeline, numeric_features),
            ("cat", categorical_pipeline, categorical_features),
        ]
    )

    # Отбор признаков на основе модели
    feature_selector = Pipeline(
        steps=[
            ("preprocessor", preprocessor),
            ("power_transform", PowerTransformer(method="yeo-johnson")),
            ("winsorizer", Winsorizer(capping_method="gaussian", tail="both", fold=3)),
            ("scaler", MinMaxScaler()),
            ("selector", SelectFromModel(LassoCV())),
        ]
    )

    data_preprocessed = feature_selector.fit_transform(features, data["HeartDisease"])
    selected_columns = feature_selector.named_steps["selector"].get_support(
        indices=True
    )
    columns = [features.columns[i] for i in selected_columns]
    df_preprocessed = pd.DataFrame(data_preprocessed, columns=columns)

    # Добавление колонки таргета обратно
    df_preprocessed["target"] = data["HeartDisease"]

    df_preprocessed.to_csv(output_filepath, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Advanced data preprocessing.")
    parser.add_argument("input", type=str, help="Filepath for the input data")
    parser.add_argument("output", type=str, help="Filepath for the output data")
    args = parser.parse_args()

    main(args.input, args.output)
