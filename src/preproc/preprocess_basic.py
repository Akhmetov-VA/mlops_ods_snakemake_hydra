import argparse

import pandas as pd
from category_encoders import CountEncoder
from feature_engine.outliers import Winsorizer
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PowerTransformer, StandardScaler


def main(input_filepath, output_filepath):
    # Чтение данных
    data = pd.read_csv(input_filepath)

    features = data.drop(columns=["HeartDisease"])

    # Разделение на числовые и категориальные переменные
    numeric_features = features.select_dtypes(include=["int64", "float64"]).columns
    categorical_features = features.select_dtypes(include=["object"]).columns

    # Создание Pipeline
    numeric_pipeline = Pipeline(
        steps=[
            ("imputer", SimpleImputer(strategy="mean")),
        ]
    )

    categorical_pipeline = Pipeline(
        steps=[
            ("imputer", SimpleImputer(strategy="constant", fill_value="missing")),
            ("count_encoder", CountEncoder(normalize=True)),
        ]
    )

    transformer = ColumnTransformer(
        transformers=[
            ("num", numeric_pipeline, numeric_features),
            ("cat", categorical_pipeline, categorical_features),
        ]
    )

    preprocessor = Pipeline(
        steps=[
            ("transformer", transformer),
            ("winsorizer", Winsorizer(capping_method="gaussian", tail="both", fold=2)),
            ("scaler", StandardScaler()),
            ("power_transform", PowerTransformer(method="yeo-johnson")),
        ]
    )

    # Обработка данных
    data_preprocessed = preprocessor.fit_transform(features)
    columns = numeric_features.tolist() + categorical_features.tolist()
    df_preprocessed = pd.DataFrame(data_preprocessed, columns=columns)

    # Добавление колонки таргета обратно
    df_preprocessed["target"] = data["HeartDisease"]

    # Сохранение обработанных данных
    df_preprocessed.to_csv(output_filepath, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Preprocess the data.")
    parser.add_argument("input", type=str, help="Filepath for the input data")
    parser.add_argument("output", type=str, help="Filepath for the output data")
    args = parser.parse_args()

    main(args.input, args.output)
