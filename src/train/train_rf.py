import argparse

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.model_selection import train_test_split


def main(input_filepath, output_filepath):
    # Загрузка данных
    data = pd.read_csv(input_filepath)

    # Разделение на признаки и целевую переменную
    X = data.drop(columns=["target"])
    y = data["target"]

    # Разделение данных на тренировочные и тестовые
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    # Создание и обучение модели случайного леса
    model = RandomForestClassifier(n_estimators=100, random_state=42)
    model.fit(X_train, y_train)

    # Оценка модели
    predictions = model.predict(X_test)
    accuracy = accuracy_score(y_test, predictions)
    roc_auc = roc_auc_score(y_test, model.predict_proba(X_test)[:, 1])

    print(f"Accuracy: {accuracy}, ROC AUC: {roc_auc}")

    # Сохранение модели
    pd.to_pickle(model, output_filepath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a random forest model.")
    parser.add_argument("input", type=str, help="Filepath for the input data")
    parser.add_argument("output", type=str, help="Filepath for the output model")
    args = parser.parse_args()

    main(args.input, args.output)
